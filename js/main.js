require([
  'js/jquery-ext',
  'js/config',
  'moment',
  'js/vendor/utility/Window',
  'js/vendor/utility/String',
  'js/vendor/utility/Log'
], function ($, config, moment, Window, String, Log) {
  $(function () {

    // Initial setup

    // jQuery Elements
    var $home = $('#home'), $employees = $('#employees'), $timesheets = $('#timesheets'), $payroll = $('#payroll'), $roleItemTemplate = $('.role-item-template'),
        $timesheetTemplate = $('.timesheet-item-template');

    var currentUser, timeSheetUser, payrollUser, payrollCompany, companies, eventId = null;

    // Tab switching
    $('.navbar-fixed-top a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });

    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd'
    });
    $('.datepicker').datepicker('update', new Date());

    $('.timepicker').timepicker();
    $('.timepicker').focus(function () {
      $(this).timepicker('showWidget');
    });

    $('.btn-pay').click(function () {
      // TODO
    });

    // Logout
    $('.btn-sign-off').click(function () {
      $.getJSON(config.logoutURL(), function (result) {
        Log.debug('Logout result', result);
        if (result.success) {
          window.location = config.clientURL();
        }
      });
    });

    // Functions

    var loggedInOrRedirect = function () {
      return $.getJSON(config.checkUserURL(),
          function (user) {
            Log.debug('User logged in', user);
            $('.user-dropdown').html(user.name);
            var firstName = user.name.match(/^\s*\w+/);
            firstName = firstName.length ? firstName[0] : null;
            firstName && $('.user-name').html(firstName);
          }).fail(function (error) {
            Log.debug('User not logged in', error);
            var redirect = Window.GET('redirect');
            if (!redirect || (redirect && parseInt(redirect) === 1)) {
              window.location = config.loginPageURL();
            }
          });
    };

    var populateHomePage = function () {
      return $.getJSON(config.serverURL() + 'users/' + currentUser.id + '/roles', function (roles) {
        var $rolesList = $('.roles-list');
        $rolesList.empty();
        Log.debug('User roles', roles);
        $.each(roles, function (i, role) {
          Log.debug('User role', role);
          var $roleItem = $roleItemTemplate.clone();
          $roleItem.removeClass('hidden');
          var name = String.camelToTitleCase(role.name);
          $('.role-title', $roleItem).html(name);
          $('.role-company', $roleItem).html(role.company ? role.company.name : '');
          $('.role-desc', $roleItem).html(role.description || '');
          $rolesList.append($roleItem);
        });
      });
    };

    var createTable = function (rowsData, columns) {
      var $table = $('<table class="table"></table>');
      var $header = $('<tr></tr>');
      for (var columnId in columns) {
        var $cell = $('<th></th>');
        var columnValue = columns[columnId];
        if (typeof columnValue == 'function') {
          $cell.html(columnId);
        } else {
          $cell.html(columnValue + '');
        }
        $header.append($cell);
      }
      $table.append($header);
      $.each(rowsData, function (i, rowData) {
        var $row = $('<tr></tr>');
        $table.append($row);
        for (var columnId in columns) {
          var columnValue = columns[columnId], cellData;
          if (typeof columnValue == 'function') {
            // TODO(aramk) use as title if string
            cellData = columnValue(rowData, columnId);
          } else {
            cellData = rowData[columnId];
          }
          var $cell = $('<td></td>');
          $row.append($cell);
          cellData = cellData === null ? 'N/A' : cellData;
          if (cellData !== undefined) {
            if (cellData instanceof $) {
              $cell.append(cellData);
            } else {
              $cell.html(cellData + '');
            }
          }
        }
      });
      return $table;
    };

    var populateEmployeesPage = function () {
      return $.getJSON(config.serverURL() + 'companies', function (result) {
        companies = result;
        Log.debug('Companies', companies);
        var $list = $('.companies-list', $employees);
        $list.empty();
        $.each(companies, function (i, company) {
          Log.debug('Company', company);
          $.getJSON(config.serverURL() + 'companies/' + company.id + '/employees', function (employees) {
            $list.append('<h2>' + company.name + '</h2>');
            Log.debug('Employees', employees);
            var users = {};
            $.each(employees, function (id, employee) {
              var user = employee.user, name = currentUser.name, id = currentUser.id;
              users[id] = users[id] || [id];
              users[id].push(user);
            });
            var $table = createTable(employees, {
              Name: function (row) {
                return row.user.name;
              },
              Role: function (row) {
                return String.camelToTitleCase(row.name);
              },
              taxFileNumber: 'TFN',
              'Hourly Wage': function (row) {
                var wage = row.hourlyWageRate;
                return wage ? '$ ' + wage : '';
              },
              'Actions': function (row) {
                var $div = $('<div class="actions"></div>');
                var $viewTimesheets = $('<a>View TimeSheets</a>');
                $viewTimesheets.click(function () {
                  timeSheetUser = row.user;
                  $('a[href="#timesheets"]').click();
                  populateTimeSheetPage();
                });
                var $viewPayroll = $('<a>View Payroll</a>');
                $viewPayroll.click(function () {
                  payrollUser = row;
                  payrollCompany = company;
                  $('a[href="#payroll"]').click();
                  populatePayrollPage();
                });
                $div.append($viewTimesheets);
                $div.append($viewPayroll);
                return $div;
              }
            });
            $list.append($table);
          });
        });
      });
    };

    var populateEmployeeModal = function () {
      var $modal = $('#modal-employee');
      $('.btn-new-employee').click(function () {
        $modal.modal();
      });
      var companySelect = $('select[name="company"]', $modal);
      companySelect.empty();
      $.each(companies, function (i, company) {
        companySelect.append('<option value="' + company.id + '">' + company.name + '</option>');
      });
      $.getJSON(config.serverURL() + 'users', function (users) {
        Log.debug('Users', users);
        var userSelect = $('select[name="user"]', $modal);
        $.each(users, function (i, user) {
          userSelect.append('<option value="' + user.id + '">' + user.name + '</option>');
        });
      });
      $('.btn-primary', $modal).click(function () {
        var data = {};
        $('[name]', $modal).each(function (i, elem) {
          data[$(elem).attr('name')] = $(elem).val();
        });
        console.error('data', data);
        var companyId = data.company;
        $.postJSON(config.serverURL() + 'companies/' + companyId + '/employees', data, function (result) {
          console.error('result', result);
          $modal.modal('hide');
          populateEmployeesPage();
        });
      });
    };

    var populateTimeSheetPage = function () {
      return $.getJSON(config.serverURL() + 'users/' + timeSheetUser.id + '/roles', function (roles) {
        $('.timesheet-user').html(timeSheetUser.name);
        Log.debug('Timesheet user roles', roles);
        var $list = $('.timesheets-list', $timesheets);
        $list.empty();
        $.each(roles, function (i, role) {
          if (role.company) {
            Log.debug('Timesheet user role', role);
            var $roleItem = $timesheetTemplate.clone();
            $roleItem.removeClass('hidden');
            var name = String.camelToTitleCase(role.name);
            $('.role-title', $roleItem).html(name);
            $('.role-company', $roleItem).html(role.company ? role.company.name : '');
            $list.append($roleItem);
            $.getJSON(config.serverURL() + 'timesheets/employee/' + role.id, function (timesheet) {
              Log.debug('Timesheet', timesheet);
              if (timesheet && timesheet.events) {
                timesheet.events = timesheet.events.sort(function (a, b) {
                  return a.startTime > b.startTime;
                });
                var $table = createTable(timesheet.events, {
                  'Start Time': function (row) {
                    return row.startTime;
                  },
                  'End Time': function (row) {
                    return row.endTime;
                  },
                  'Type': function (row) {
                    return String.camelToTitleCase(row.type);
                  },
                  'Description': function (row) {
                    return row.description;
                  },
                  'Actions': function (row) {
                    var $div = $('<div></div>');
                    var $deleteButton = $('<button type="button" class="close" title="Delete Event" aria-hidden="true">&times;</button>');
                    $deleteButton.click(function () {
                      $.ajax({
                        url: config.serverURL() + 'events/' + row.id,
                        type: 'DELETE'
                      }).always(function () {
                            populateTimeSheetPage();
                          });
                    });
                    var $editButton = $('<button type="button" class="edit close" title="Edit Event" aria-hidden="true"><i class="glyphicon glyphicon-edit"></i></button>');
                    $editButton.click(function () {
                      var $modal = $('#modal-event');
                      $modal.modal('show');
                      $.getJSON(config.serverURL() + 'events/' + row.id, function (event) {
                        $('[name="description"]', $modal).val(event.description);
                        $('[name="start-time"]', $modal).val(moment(event.startTime).format('h:mm:ss a'));
                        $('[name="start-date"]', $modal).val(moment(event.startDate).format('YYYY-MM-DD'));
                        $('[name="end-time"]', $modal).val(moment(event.endTime).format('h:mm:ss a'));
                        $('[name="end-date"]', $modal).val(moment(event.endDate).format('YYYY-MM-DD'));
                        $('[name="type"]', $modal).val(event.type);
                        eventId = row.id;
                        console.error('event', event);
                      });
                    });
                    $div.append($editButton);
                    $div.append($deleteButton);
                    return $div;
                  }
                });

                var $addButton = $('<button class="btn btn-success btn-new-event">Add Event</button>');
                var $modal = $('#modal-event');
                $addButton.click(function () {
                  eventId = null;
                  $modal.modal();
                });
                $modal.attr('data-employee-id', role.id);
                $roleItem.append($addButton);
                $roleItem.append($table);
              } else {
                $roleItem.append($('<p class="timesheet-content">No TimeSheets.</p>'));
              }
            });
          }
        });
      });
    };

    // TODO(aramk) put in a utility file
    var toISODateTimeString = function (date, time) {
      if (!date || !time) {
        return '';
      }
      date = moment(date + '');
      var timeDate = moment(time, 'h:mm:ss a');
      if (timeDate) {
        date.hour(timeDate.hour());
        date.minute(timeDate.minute());
        date.second(timeDate.second());
        return date.toDate().toISOString();
      } else {
        return null;
      }
    };

    var populatePayrollPage = function () {
      return $.postJSON(config.serverURL() + 'payroll/calculate', {
        employeeId: payrollUser.id
      }, function (results) {
        Log.debug('Payroll', results);
        var $results = $('.payroll-results', $payroll);
        $results.empty();
        console.error('payrollUser', payrollUser);
        var $info = $('<p>Payroll for employee ' + payrollUser.user.name + ' of company ' + payrollCompany.name + '.</p>');
        $results.append($info);
        var $table = $('<table></table>');
        $results.append($table);
        for (var field in results) {
          var value = results[field];
          var $row = $('<tr><td><strong>' + field + '</strong></td><td>' + value.toFixed(2) + '</td></tr>');
          $table.append($row);
        }
      });
    };

    var populateEventModal = function () {
      var $modal = $('#modal-event');
      // TODO
      $('.btn-primary', $modal).click(function () {
        var employeeId = $modal.attr('data-employee-id');
        console.error('employeeId..', employeeId);
//        $('[name]', $modal).each(function (i, elem) {
//          data[$(elem).attr('name')] = $(elem).val();
//        });

        var startDate = $('[name="start-date"]', $modal).datepicker('getDate'),
            endDate = $('[name="end-date"]', $modal).datepicker('getDate'),
            startTime = $('[name="start-time"]').val(),
            endTime = $('[name="end-time"]').val(),
            startDateTime = toISODateTimeString(startDate, startTime),
            endDateTime = toISODateTimeString(endDate, endTime);

        var data = {
          type: $('[name="type"]', $modal).val(),
          description: $('[name="description"]', $modal).val(),
          startTime: startDateTime,
          endTime: endDateTime
        };
        console.error('data', data);

        if (eventId === null) {
          $.postJSON(config.serverURL() + 'timesheets/employee/' + employeeId, data, function (result) {
            console.error('result', result);
            $modal.modal('hide');
            populateTimeSheetPage();
          });
        } else {
          data.id = eventId;
          $.ajax({
            url: config.serverURL() + 'events/' + eventId,
            method: 'PUT',
            data: JSON.stringify(data),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
          }).always(function () {
                $modal.modal('hide');
                populateTimeSheetPage();
              });
        }
      });
    };

    // Load pages

    loggedInOrRedirect().done(function (user) {
      currentUser = user;
      timeSheetUser = user;
      populateHomePage();
      populateEmployeesPage().done(populateEmployeeModal);
//      populateTimeSheetPage(currentUser);
      // TODO(aramk) make user this changeable
      $.getJSON(config.serverURL() + 'users/2', function (user) {
        timeSheetUser = user;
        populateTimeSheetPage();
      });
      populateEventModal();
//      populatePayrollPage();
    });

  });
});
