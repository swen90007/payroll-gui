define(['js/vendor/utility/Window'], function (Window) {
  return {
    serverSuffix: 'akda.pay',
    serverURL: function () {
      return (Window.GET('serverURL') || Window.currentHost() + this.serverSuffix) + '/';
    },
    loginURL: function () {
      return this.serverURL() + 'resources/j_spring_security_check';
    },
    logoutURL: function () {
      return this.serverURL() + 'resources/j_spring_security_logout';
    },
    checkUserURL: function () {
      return this.serverURL() + 'users/currentUserDetails';
    },
    clientURL: function () {
      return Window.currentDir();
    },
    loginPageURL: function () {
      return this.clientURL() + 'login.html';
    }
  };
});
