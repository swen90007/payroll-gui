require([
  'js/jquery-ext',
  'js/config',
  'js/vendor/utility/Window',
  'js/vendor/utility/Log'
], function ($, config, Window, Log) {
  var $btnLogin = $('.login-container .btn'),
      $msg = $('.alert-danger');
  $msg.hide();
  $msg.removeClass('hidden');
  $btnLogin.click(function () {
    $.post(config.loginURL(), {
      j_username: $('.field-username').val(),
      j_password: $('.field-password').val()
    }, function (result) {
      Log.debug('Login result', result);
      if (result.success) {
        window.location = config.clientURL();
        $msg.fadeOut();
      } else {
        $msg.fadeIn();
        setTimeout(function () {
          $msg.fadeOut();
        }, 3000);
      }
    }, 'json');
  });
  $('.auth-form').bind('keypress', function (e) {
    if (e.keyCode == 13) {
      $btnLogin.click();
    }
  });
});
