define(['jquery'], function ($) {
  /**
   * A set of jQuery extensions.
   */

  $.postJSON = function(url, data, success, args) {
    args = $.extend({
      url: url,
      type: 'POST',
      data: JSON.stringify(data),
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      async: true,
      success: success
    }, args);
    return $.ajax(args);
  };

  return $;

});
